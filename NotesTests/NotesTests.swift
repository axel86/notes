//
//  NotesTests.swift
//  NotesTests
//
//  Created by Алексей Лопух on 29.04.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import XCTest
@testable import Notes


class NotesTests: XCTestCase {
    
    var notebookTest1: DummyNotebook!
    var notebookTest2: DummyNotebook!
    
    var noteA: Note!
    var noteB: Note!
    var noteC: Note!
    
    override func setUp() {
        super.setUp()
        notebookTest1 = DummyNotebook()
        notebookTest2 = DummyNotebook()
        
        noteA = Note(title: "hell", content: "hello", color: UIColor.blue)
        noteB = Note(title: "qwert", content: "qwerty")
        noteC = Note(title: "hey", content: "My name is Skrillex", color: UIColor.red, dateDes: nil)
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        notebookTest1 = nil
        notebookTest2 = nil
        noteA = nil
        noteB = nil
        noteC = nil
    }

    //добавляем дважды одну и ту же заметку и видем, что второй раз она не добавилась
    func testAddNoteInNotebook(){
        let addNoteAOne = notebookTest1.addNote(newValue: noteA)
        
        let addNoteATwo = notebookTest1.addNote(newValue: noteA)
        
        XCTAssertTrue(addNoteAOne && addNoteATwo == false)
    }
    
    func testSaveDataAndLoadData(){
        var count:Int = 0
        count += (notebookTest1.addNote(newValue: noteA)) ? 1 : 0
        count += (notebookTest1.addNote(newValue: noteC)) ? 1 : 0
        count += (notebookTest1.addNote(newValue: noteB)) ? 1 : 0
        
        //получилось сохранить
        XCTAssertTrue(notebookTest1.saveData())
        
        // не добавилось новых заметок
        XCTAssertTrue(notebookTest1.loadData() == 0)
        
    
        // видем что все три заметки успешно добавились в другой пустой Notebook
        XCTAssertTrue(notebookTest2.loadData() == count)
    }
    
    func testParseJson(){
        let testParse = Note.parse(json: ["title": "hell", "content":"hello", "uuid":"1234", "color":[1.0, 1.0]])
        let testNote = Note(title: "hell", content: "hello", uuid: "1234", color: UIColor.black, dateDes: nil)
        
        print(testParse!)
        print(testNote)
        XCTAssertTrue(testNote == testParse!)
        
        
    }
}
