//
//  AnimatedTransition.swift
//  Notes
//
//  Created by Алексей Лопух on 17.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

class AnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning {

    var isPresentation: Bool = false
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from) ,
            let toVC = transitionContext.viewController(forKey: .to) else
        {return}
        let fromView = fromVC.view!
        let toView = toVC.view!
        let containerView = transitionContext.containerView
        
        if isPresentation{
            containerView.addSubview(toView)
        }
        
        let animatingVC = isPresentation ? toVC : fromVC
        let animatingView = animatingVC.view!
        
        let finalFrameForVC = transitionContext.finalFrame(for: animatingVC)
        var initialFrameForVC = finalFrameForVC
        
        
        initialFrameForVC.origin.y += 75
        
        let initialFrame = isPresentation ? initialFrameForVC : finalFrameForVC
        let finalFrame = isPresentation ? finalFrameForVC : initialFrameForVC
        
        animatingView.frame = initialFrame
        animatingView.alpha = isPresentation ? 0.0 : 1.0
        
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext),
                       delay: 0.0,
                       usingSpringWithDamping: 300.0,
                       initialSpringVelocity: 5.0,
                       options: UIViewAnimationOptions.allowUserInteraction ,
                       animations: {
                        animatingView.frame = finalFrame
                        animatingView.alpha = self.isPresentation ? 1.0 : 0.0
        }, completion: {(value: Bool) in
            if !self.isPresentation{
                fromView.removeFromSuperview()
            }
            transitionContext.completeTransition(true)
        })
    }
}
