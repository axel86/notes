//
//  CrNote+CoreDataProperties.swift
//  Notes
//
//  Created by Алексей Лопух on 08.06.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import Foundation
import CoreData


extension CrNote {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CrNote> {
        return NSFetchRequest<CrNote>(entityName: "CrNote")
    }

    @NSManaged public var color: String?
    @NSManaged public var content: String?
    @NSManaged public var destroy_date: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var uid: String?

}
