//
//  Note.swift
//  Notes
//
//  Created by Алексей Лопух on 20.04.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import Foundation
import UIKit

struct Note {
    let color: UIColor
    let dateDestroy: Date?
    let title:String
    let content: String
    
    let uuid: String
    
    
    init(title: String, content: String, uuid: String = UUID().uuidString.lowercased(), color: UIColor = UIColor.white, dateDes: Date? = nil) {
        self.uuid = uuid
        self.color = color
        self.dateDestroy = dateDes
        self.content = content
        self.title = title
    }
    
    static func ==(left: Note, rigth: Note) -> Bool {
        return left.uuid == rigth.uuid && left.color == rigth.color &&
        left.dateDestroy == rigth.dateDestroy && left.content == rigth.content &&
        left.title == rigth.title
    }
    
    static func !=(left: Note, rigth: Note) -> Bool {
        return !(left.uuid == rigth.uuid && left.color == rigth.color &&
            left.dateDestroy == rigth.dateDestroy && left.content == rigth.content &&
            left.title == rigth.title)
    }
}
