//
//  Dispatcher.swift
//  Notes
//
//  Created by Алексей Лопух on 28.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
enum QueueOperation {
    case main, url, cach, background
}

class Dispatcher{
    
    private let main = OperationQueue.main
    
    private let cacheQueue = OperationQueue()
    private let URLQueue = OperationQueue()
    private let backgroundQueue = OperationQueue()
    
    init() {
        cacheQueue.maxConcurrentOperationCount = 5
        
        URLQueue.maxConcurrentOperationCount = 5
        
        backgroundQueue.maxConcurrentOperationCount = 5
    }
    
    func addOperatino(operation: MyOperation, queue: QueueOperation){
        if let completion = operation.completion{
            let newOperation = MyOperation(action: completion, completion: nil)
            newOperation.addDependency(operation)
            main.addOperation(newOperation)
        }
        switch queue {
        case .background:
            backgroundQueue.addOperation(operation)
        case .cach:
            cacheQueue.addOperation(operation)
        case .main:
            main.addOperation(operation)
        case .url:
            URLQueue.addOperation(operation)
        }
    }
}
