//
//  Color+hexString.swift
//  Notes
//
//  Created by Алексей Лопух on 10.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
extension UIColor {
    public func hexString() -> String{
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: nil)
        return String(format: "#%02X%02X%02X", Int(r * 0xFF), Int(g * 0xFF), Int(b * 0xFF))
    }
    
    convenience init(hexString: String) {
        var cleanedHexString = hexString
        if hexString.hasPrefix("#") {
            cleanedHexString = String(hexString.characters.dropFirst())
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cleanedHexString).scanHexInt32(&rgbValue)
        
        let red = CGFloat((rgbValue >> 16) & 0xff) / 255.0
        let green = CGFloat((rgbValue >> 08) & 0xff) / 255.0
        let blue = CGFloat((rgbValue >> 00) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
}
