//
//  ColorNote.swift
//  Notes
//
//  Created by Алексей Лопух on 09.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

class ColorNote: UIView {
    var isCheck: Bool = false {
        didSet{
            setNeedsDisplay()
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {return}
        
        context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(2.0)
        context.stroke(rect)
        
        if isCheck{
            let minSize = min(rect.height / 3, rect.width / 3)
            let rectCircle = CGRect(x: rect.width / 3 * 2 - 3,
                                    y: 0,
                                    width: minSize,
                                    height: minSize)
            
            context.strokeEllipse(in: rectCircle)
            
            context.move(to: CGPoint(x: rectCircle.minX  + 1 , y: rectCircle.height / 2))
            context.addLine(to: CGPoint(x: rectCircle.minX + (rectCircle.width / 2) , y: rectCircle.maxY - 1))
            
            context.addLine(to: CGPoint(x: rectCircle.maxX - 3, y: rectCircle.minY + 3))
            context.strokePath()
        }
        
    }
}
