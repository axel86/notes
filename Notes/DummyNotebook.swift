//
//  DummyNotebook.swift
//  Notes
//
//  Created by Алексей Лопух on 20.04.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import Foundation
import UIKit

class DummyNotebook{
    private(set) var notebook:[Note] = []
    
    static var filePath: String? {
        guard let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask, true).first else{
            return nil
        }
        let path = "\(dir)/notes.plist"
        print(path)
        
        return path
    }
    
    func addNote(newValue:Note) -> Bool {
        for item in notebook{
            if item == newValue{
                return false
            }
        }
        notebook.append(newValue)
        return true
    }
    func updateNote(newValue:Note){
        for (index, item) in notebook.enumerated(){
            if item.uuid == newValue.uuid{
                notebook[index] = newValue
                return
            }
        }
        notebook.append(newValue)
    }
    
    func removeNote(uid:String) -> Note? {
        var indexRemove:Int = -1
        for (index,note) in notebook.enumerated() {
            if note.uuid == uid {
                indexRemove = index
                break
            }
        }
        if indexRemove == -1 {
            return nil
        }
        let noteRemove = notebook[indexRemove]
        notebook.remove(at: indexRemove)
        return noteRemove
        
    }
    
    func saveData() -> Bool {
        guard let path = DummyNotebook.filePath else {
            return false
        }
        
        var notes = [Any]()
        for item in notebook {
            notes.append(item.json as Any)
        }
        
        let file = notes as NSArray
        file.write(toFile: path, atomically: true)
        return true
    }
    
    //возвращает сколько объектов добавлено
    func loadData() -> Int {
        var count = 0
        guard let path = DummyNotebook.filePath else {
            return count
        }
        let dateFromFile = NSArray(contentsOfFile: path)
        guard let date = dateFromFile else {
            return count
        }
        
        for item in date {
            // проверяем можно ли откастить с json
            if let item = item as? [String:Any] {
                // проверяем можно ли распарсить этот json
                if let note = Note.parse(json: item) {
                    count = addNote(newValue: note) ? count + 1 : count
                }
            }
        }
        
        return count
    }
    
}
