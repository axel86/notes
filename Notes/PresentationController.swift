//
//  PresentationController.swift
//  Notes
//
//  Created by Алексей Лопух on 17.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

class PresentationController: UIPresentationController, UIAdaptivePresentationControllerDelegate {

    var chromeView: UIView! = UIView()
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        chromeView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        chromeView.alpha = 0.0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.chromeViewTapped(gesture:)))
        chromeView.addGestureRecognizer(tap)
    }
    override var frameOfPresentedViewInContainerView: CGRect{
        var presentedViewFrame = CGRect.zero
        guard let containerView = containerView else {
            return presentedViewFrame
        }
        let containerBounds = containerView.bounds
        presentedViewFrame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
        presentedViewFrame.origin.x = containerView.center.x - presentedViewFrame.size.width / 2
        presentedViewFrame.origin.y = containerView.center.y - presentedViewFrame.size.height / 2

        
        return presentedViewFrame
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSize(width: parentSize.width * 0.8, height: parentSize.height * 0.8)
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        guard let containerView = self.containerView else {return}
        chromeView.frame = containerView.bounds
        chromeView.alpha = 0.0
        containerView.insertSubview(chromeView, at: 0)
        if let coordinator = presentedViewController.transitionCoordinator{
            coordinator.animate(alongsideTransition: {
                (context: UIViewControllerTransitionCoordinatorContext) -> Void in
                    self.chromeView.alpha = 1.0
            }, completion: nil)
        }else{
            chromeView.alpha = 1.0
        }
        
    }
    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        if let coordinator = presentedViewController.transitionCoordinator {
            coordinator.animate(alongsideTransition: {
                (context: UIViewControllerTransitionCoordinatorContext) -> Void in
                self.chromeView.alpha = 0.0
            }, completion: nil)
        }else{
            chromeView.alpha = 0.0
        }
    }
    override func containerViewWillLayoutSubviews() {
        guard let containerView = self.containerView else {return}
        chromeView.frame = containerView.bounds
        presentedView!.frame = frameOfPresentedViewInContainerView
    }
    override var shouldPresentInFullscreen: Bool{
        return true
    }
    
    override var adaptivePresentationStyle: UIModalPresentationStyle{
        return UIModalPresentationStyle.fullScreen
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.overFullScreen
    }
    func chromeViewTapped(gesture: UIGestureRecognizer){
        if (gesture.state == UIGestureRecognizerState.ended){
            presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
}
