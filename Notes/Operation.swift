//
//  Operation.swift
//  Notes
//
//  Created by Алексей Лопух on 28.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
class MyOperation : Operation {
    
    var action: (()->())!
    var completion: (()->())? = nil
    
    fileprivate var _executing = false
    fileprivate var _finished = false
    
 
    init(action: @escaping (() -> ()), completion: (()->())?) {
        self.action = action
        self.completion = completion
    }
    
    override func start() {
        guard !isCancelled else {
            finish()
            return
        }
        
        willChangeValue(forKey: "isExecuting")
        _executing = true
        main()
        didChangeValue(forKey: "isExecuting")
    }
    
    override func main() {
        guard !isCancelled else{
            finish()
            return
        }
        action()
        
        finish()
    }
    
    fileprivate func finish() {
        willChangeValue(forKey: "isFinished")
        _finished = true
        didChangeValue(forKey: "isFinished")
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    override var isFinished: Bool {
        return _finished
    }
}
