//
//  ColorCheck.swift
//  Notes
//
//  Created by Алексей Лопух on 09.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import Foundation

enum ColorCheck{
    case Red, Green, White, Custom
}
