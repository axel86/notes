//
//  ColorPickerController.swift
//  Notes
//
//  Created by Алексей Лопух on 10.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

class ColorPickerController: UIViewController, ColorPickerDelegate {
    
    weak internal var delegate: ColorPickerControllerDelegate?
    
    @IBOutlet weak var colorPicker: ColorPickerView!
    @IBOutlet weak var colorViewCheck: ColorViewCheck!
    @IBOutlet weak var sliderBrightness: UISlider!
    
    var color: UIColor? = nil
    var point: CGPoint? = nil
    
    init(){
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorPicker.delegate = self
    
        if let color = self.color{
            colorPicker.color = color
            colorViewCheck.color = color
            var brightness = CGFloat(0)
            color.getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
            colorPicker.brightness = brightness
            sliderBrightness.value = Float(brightness)
        }else{
            let color = colorPicker.getColorAtPoint(point: CGPoint(x: colorPicker.frame.width / 2,
                                                                  y: colorPicker.frame.height / 2))
            colorViewCheck.color = color
            self.color = color
        }
        
        

    }
    
    func changeSlider(){
        var brightness = CGFloat(0)
        color?.getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
        sliderBrightness.value = Float(brightness)
    }
    
    @IBAction func sliderValueChange(_ sender: UISlider) {
        colorPicker.brightness = CGFloat(sender.value)
        var hue = CGFloat(0)
        var saturate = CGFloat(0)
        self.color?.getHue(&hue, saturation: &saturate, brightness: nil, alpha: nil)
        let newColor = UIColor(hue: hue, saturation: saturate, brightness: CGFloat(sender.value), alpha: 1.0)
        colorViewCheck.color = newColor
        colorPicker.color = newColor
    }

    @IBAction func dismissVC(_ sender: Any) {
        self.color = colorPicker.color
        self.point = colorPicker.point
        delegate?.setColor(color: self.color!)
        delegate?.setPoint(point: self.point!)

        presentingViewController!.dismiss(animated: true, completion: nil)
    }
    
    func colorPickerTouched(sender: ColorPickerView, color: UIColor, point: CGPoint, state: UIGestureRecognizerState) {
        colorViewCheck.color = color
        self.color = color
        self.point = point
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
