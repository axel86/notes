//
//  ColorPickerView.swift
//  Notes
//
//  Created by Алексей Лопух on 10.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
internal protocol ColorPickerDelegate: NSObjectProtocol{
    func colorPickerTouched(sender:ColorPickerView, color:UIColor, point:CGPoint, state:UIGestureRecognizerState)
}

@IBDesignable class ColorPickerView: UIView {
    
    weak internal var delegate: ColorPickerDelegate?
    let saturationExponentTop:Float = 2.0
    let saturationExponentBottom:Float = 1.3
    
    var brightness: CGFloat = 0.5 {
        didSet{
            var hue = CGFloat(0)
            var saturate = CGFloat(0)
            color?.getHue(&hue, saturation: &saturate, brightness: nil, alpha: nil)
            self.color = UIColor(hue: hue, saturation: saturate, brightness: brightness, alpha: 1.0)
            image = nil
            setNeedsDisplay()
        }
    }
    
    
    var point: CGPoint! = nil {
        didSet{
            if point.x < 0 {
                point.x = 0
            }else if point.x > self.frame.width {
                point.x = self.frame.width
            }
            if point.y < 0 {
                point.y = 0
            }else if point.y > self.frame.height{
                point.y = self.frame.height
            }
            setNeedsDisplay()
        }
    }
    var image: CGImage? = nil
    var color: UIColor? = nil
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    private func initialize() {
        
        self.clipsToBounds = true
        let touchGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.touchedColor(gestureRecognizer:)))
        touchGesture.minimumPressDuration = 0
        touchGesture.allowableMovement = CGFloat.greatestFiniteMagnitude
        self.addGestureRecognizer(touchGesture)

        
    }
    
    override func draw(_ rect: CGRect) {
        if point == nil {
            if let color = self.color{
                self.point = getPointForColor(color: color)
            }else{
                point = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
                color = getColorAtPoint(point: point)
            }
        }
        
        guard let context = UIGraphicsGetCurrentContext() else {return}
        if let image = self.image{
            UIImage(cgImage: image).draw(in: rect)
        }else{
        for y in stride(from: rect.minY, to: rect.height, by: 1.0){
            let saturation = (rect.height - y) / rect.height
            for x in stride(from: rect.minX, to: rect.width, by: 1.0){
                let hue = x / rect.width
                let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
                context.setFillColor(color.cgColor)
                context.fill(CGRect(x: x, y: y, width: 1.0, height: 1.0))
            }
        }
            image = context.makeImage()
        }
        let rectPoint = CGRect(x: point.x - 10, y: point.y - 10, width: 20, height: 20)
        context.setLineWidth(1.0)
        context.setStrokeColor(UIColor.black.cgColor)
        context.strokeEllipse(in: rectPoint)
        let arrayPoint: [(CGFloat, CGFloat, CGFloat, CGFloat)] = [(-10, -10, -13, -13),
                          (-10, 10, -13, 13),
                          (10,-10, 13, -13),
                          (10, 10, 13, 13)]
        for (dx, dy, addX, addY) in arrayPoint {
            context.move(to: CGPoint(x: point.x + dx, y: point.y + dy))
            context.addLine(to: CGPoint(x: point.x + addX, y: point.y + addY))
        }
        context.strokePath()
        context.setFillColor((self.color?.cgColor)!)
        context.fillEllipse(in: rectPoint)
        
        context.setLineWidth(2.0)
        context.setStrokeColor(UIColor.black.cgColor)
        context.stroke(rect)
        
    }
    
    func getColorAtPoint(point:CGPoint) -> UIColor {
        let roundedPoint = CGPoint(x:1.0 * CGFloat(Int(point.x / 1.0)),
                                   y:1.0 * CGFloat(Int(point.y / 1.0)))
        let saturation = (self.bounds.height - roundedPoint.y) / self.bounds.height
        let hue = roundedPoint.x / self.bounds.width
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    func getPointForColor(color:UIColor) -> CGPoint {
        var hue:CGFloat=0;
        var saturation:CGFloat=0;
        color.getHue(&hue, saturation: &saturation, brightness: nil, alpha: nil);
        
        
        let yPos = self.bounds.height - (saturation * self.bounds.height)
        let xPos = hue * self.bounds.width
        
        return CGPoint(x: xPos, y: yPos)
    }
    func touchedColor(gestureRecognizer: UILongPressGestureRecognizer){
        let point = gestureRecognizer.location(in: self)
        let color = getColorAtPoint(point: point)
        self.point = point
        self.color = color
        
        self.delegate?.colorPickerTouched(sender: self, color: color, point: point, state:gestureRecognizer.state)
    }

}
