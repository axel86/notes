//
//  ColorViewCheck.swift
//  Notes
//
//  Created by Алексей Лопух on 10.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

@IBDesignable
class ColorViewCheck: UIView {

    @IBOutlet weak var colorViewCheck: UIView!
    @IBOutlet weak var colorName: UILabel!
    
    var color: UIColor = UIColor.black{
        didSet{
            colorName.text = color.hexString()
            colorViewCheck.backgroundColor = color
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    func commonInit() {
        guard let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last else {
            return
        }
        guard let nibView = Bundle(for: type(of: self)).loadNibNamed(nibName, owner: self, options: nil)?.last as? UIView else {
            return
        }
        
        addSubview(nibView)
        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        colorViewCheck.layer.cornerRadius = 10
    }
    

}
