//
//  NotesViewController.swift
//  Notes
//
//  Created by Алексей Лопух on 14.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
import CoreData

class NotesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var slider: UISlider!
    
    let core = CoreDataManager()
    let dispatcher = Dispatcher()
    var urlOper: URLOperation!
    
    let layoutMinimumLineSpacing: CGFloat = 5
    let layoutMinimumInteritemSpacing: CGFloat = 5
    var fetchResultController: NSFetchedResultsController<NSFetchRequestResult>!
    
    var notes: [Note] = []
    var isDelete: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.urlOper = URLOperation()
        self.urlOper.delegate = self
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CrNote")
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        self.fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataManager.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchResultController.delegate = self
        
        setupNotebook()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        changeValue(slider)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        changeValue(slider)
    }
    @IBAction func changeValue(_ sender: Any) {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = layoutMinimumLineSpacing
        layout.minimumInteritemSpacing = layoutMinimumInteritemSpacing
        
        let value = CGFloat((sender as! UISlider).value) / 100
        let change = (self.collectionView.frame.height < self.collectionView.frame.width) ? CGFloat(1.0) : CGFloat(2.5)
        
        let size: CGSize = CGSize(width:self.collectionView.frame.width * value ,
                                  height: self.collectionView.frame.height * value / change)
        
        layout.itemSize = size
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        let cells = collectionView.visibleCells as! [noteCell]
        for cell in cells{
            cell.isDelete = self.isDelete
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "new" {
            let vc = segue.destination as! NotesEditor
            vc.delegat = self
            vc.note = nil
        }
    }
    @IBAction func editCell(_ sender: Any) {
        self.navigationItem.leftBarButtonItem?.title = isDelete ? "Edit" : "Done"
        isDelete = isDelete ? false : true
        collectionView.reloadData()
        collectionView.performBatchUpdates(nil, completion: nil)
    }
}
extension NotesViewController: UICollectionViewDelegate, UICollectionViewDataSource, NotesEditorDelegat, URLOperationDelegate, NSFetchedResultsControllerDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! noteCell
        
        cell.setNote(note: notes[indexPath.row])
        cell.backgroundColor = UIColor.lightGray
        
        cell.isDelete = isDelete

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let c = cell as! noteCell
        c.isDelete = isDelete
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isDelete{
            let deleteIndex = indexPath.row
            let deleteNote = self.notes[deleteIndex]
            
            let delete = MyOperation(action: {
                self.notes.remove(at: deleteIndex)
                CoreDataManager.managedObjectContext.performAndWait {
                    
                    CoreDataManager.managedObjectContext.delete(self.fetchResultController.object(at: indexPath) as! NSManagedObject)
                    do {
                        try CoreDataManager.managedObjectContext.save()
                    } catch {
                        print("error")
                    }
                }
            }, completion: {
                self.collectionView.reloadData()
                self.collectionView.performBatchUpdates(nil, completion: nil)
            })
            
            
            let deleteUrl = MyOperation(action: {
                NSLog("\(deleteNote.json)")
                self.urlOper.httpRequest(setting: .delete, uid: "/"+deleteNote.uuid, note: nil)
                
            }, completion: nil)
            
            
            dispatcher.addOperatino(operation: deleteUrl, queue: .url)
            dispatcher.addOperatino(operation: delete, queue: .cach)
        }else{
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "change") as! NotesEditor
            if let cell = collectionView.cellForItem(at: indexPath) as? noteCell{
                vc.note = cell.note
                vc.delegat = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func setupNotebook(){
        let url = MyOperation(action: {
            self.urlOper.httpRequest(setting: .get, uid: "", note: nil)
        }, completion: nil)
        dispatcher.addOperatino(operation: url, queue: .url)
        
    }
    
    func addNewNote(newNote: Note) {
        var removeIndex:Int? = nil
        for (index, note) in self.notes.enumerated(){
            if note.uuid == newNote.uuid{
                removeIndex = index
            }
        }
        NSLog("\(newNote.json)")
        let update = MyOperation(action: {
            
            if let index = removeIndex{
                self.notes.remove(at: index)
                self.notes.insert(newNote, at: index)
                CoreDataManager.managedObjectContext.delete(self.fetchResultController.object(at: IndexPath(row: index, section: 0)) as! NSManagedObject)
            }else{
                self.notes.append(newNote)
            }
            self.saveContext()
        }, completion: nil)
        let updateUrl = MyOperation(action: {
            if let _ = removeIndex{
                self.urlOper.httpRequest(setting: .put, uid: "/"+newNote.uuid, note: newNote)
            }else{
                self.urlOper.httpRequest(setting: .post, uid: newNote.uuid, note: newNote)
            }
        }, completion: {
            self.collectionView.reloadData()
            self.collectionView.performBatchUpdates(nil, completion: nil)
        }
        )
        updateUrl.addDependency(update)
        dispatcher.addOperatino(operation: updateUrl, queue: .url)
        dispatcher.addOperatino(operation: update, queue: .cach)
    }
    func updateNotes(notes: [Note], setting: RequestSetting) {
        switch setting {
        case .get:
            self.notes = notes
            self.saveContext()
            self.collectionView.reloadData()
            self.collectionView.performBatchUpdates(nil, completion: nil)
        case .put:
            self.addNote(note: notes.first!)
        default: DispatchQueue.global().async {
            CoreDataManager.managedObjectContext.performAndWait {
                do {
                    try self.fetchResultController.performFetch()
                    let res = self.fetchResultController.fetchedObjects as? [CrNote]
                    let result = res?.flatMap({ crNote -> Note in
                        return self.crNoteTONote(crNote: crNote)
                        
                    })
                    if let result = result{
                        self.notes = result
                    }
                } catch {
                    print("error")
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.collectionView.performBatchUpdates(nil, completion: nil)
                }
            }
            }
        }
        
    }
    func saveContext(){
        CoreDataManager.managedObjectContext.performAndWait {
            do{
                try self.fetchResultController.performFetch()
            } catch{
                print("error")
            }
            if let result = self.fetchResultController.fetchedObjects as? [NSManagedObject]{
                for item in result{
                    CoreDataManager.managedObjectContext.delete(item)
                }
            }
            for item in self.notes{
                let entity = NSEntityDescription.entity(forEntityName: "CrNote", in: CoreDataManager.managedObjectContext)
                let note = CrNote(entity: entity!, insertInto: CoreDataManager.managedObjectContext)
                note.color = item.color.hexString()
                note.content = item.content
                note.destroy_date = item.dateDestroy as NSDate?
                note.title = item.title
                note.uid = item.uuid
            }
            do
            {
                try CoreDataManager.managedObjectContext.save()
            }catch{
                print("error")
            }
        }
    }
    private func addNote(note: Note){
        var removeIndex: Int? = nil
        for (index, item) in self.notes.enumerated(){
            if note.uuid == item.uuid{
                removeIndex = index
            }
        }
        if let index = removeIndex{
            self.notes.remove(at: index)
            self.notes.insert(note, at: index)
        }
        self.collectionView.reloadData()
        self.collectionView.performBatchUpdates(nil, completion: nil)
    }
    func crNoteTONote(crNote: CrNote) ->Note {
        return Note(title: crNote.title!,
                    content: crNote.content!,
                    uuid: crNote.uid!,
                    color: UIColor(hexString: crNote.color!),
                    dateDes: crNote.destroy_date as Date?)
    }
}
