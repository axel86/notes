//
//  URLOperation.swift
//  Notes
//
//  Created by Алексей Лопух on 03.06.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
enum RequestSetting {
    case get, post, put, delete
}
internal protocol URLOperationDelegate: NSObjectProtocol{
    func updateNotes(notes: [Note], setting: RequestSetting)
}
class URLOperation: NSObject {
    weak internal var delegate: URLOperationDelegate?
    
    private var isFinish: Bool = true
    private var requestQueue: [URLRequest] = []
    private var viewController: NotesViewController!
    private var notes: [Note]?
    private let baseURLString = "http://notes.mrdekk.ru/notes"
    private lazy var HTTPHeaders: [String: String] = {
        let token = "Abracadabra"
        var defaultHeaders: [String : String] = ["Content-Type" : "application/json; charset=utf-8"]
        
        defaultHeaders["Authorization"] = "Bearer " + token
        return defaultHeaders
    }()
    
    static let session: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = URLCache(memoryCapacity: 100 * 1024 * 1024,
                                          diskCapacity: 100 * 1024 * 1024,
                                          diskPath: nil)
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return URLSession(configuration: configuration, delegate: self as? URLSessionDelegate, delegateQueue: .main)
    }()
    // GET http://notes.mrdekk.ru/notes
    // GET http://notes.mrdekk.ru/notes/<uid>
    // POST http://notes.mrdekk.ru/notes + заметка
    // PUT http://notes.mrdekk.ru/notes/<uid> + заметка
    // DELETE http://notes.mrdekk.ru/notes/<uid>
    func httpRequest(setting: RequestSetting, uid: String, note: Note?){
        var httpMethod = ""
        var path = uid
        var message:Note? = nil
        switch setting {
        case .delete:
            httpMethod = "DELETE"
        case .get:
            httpMethod = "GET"
        case .post:
            httpMethod = "POST"
            path = ""
            message = note
        case .put:
            httpMethod = "PUT"
            message = note
        }
        request(httpMethod: httpMethod, path: path, message: message)
        if isFinish{
            startTask()
        }
    }
    private func parseData(httpMethod: String, data: Data){
        let json = try? JSONSerialization.jsonObject(with: data, options: [])
        if let js = json as? [[String: Any]]{
            self.notes = js.flatMap({Note.parse(json: $0)})
        }else if let js = json as? [String:Any]{
            if let note = Note.parse(json: js){
                self.notes = [note]
            }else{
                self.notes = nil
            }
        }else{
            self.notes = nil
        }
    }
    private func request(httpMethod: String, path: String, message: Note?){
        let urlstr = baseURLString + path
        guard let url = URL(string: urlstr) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.allHTTPHeaderFields = HTTPHeaders
        if let message = message{
            request.httpBody = try! JSONSerialization.data(withJSONObject: message.json)
        }
        requestQueue.append(request)
    }
    private func startTask(){
        if let request = self.requestQueue.first{
            isFinish = false
            let task = URLOperation.session.dataTask(with: request){
                data, response, error in
                if let error = error{
                    NSLog(error.localizedDescription)
                    self.delegate?.updateNotes(notes: [], setting: .delete)
                    return
                }
                if let response = response as? HTTPURLResponse{
                    NSLog("Status: \(response.statusCode)")
                    switch response.statusCode{
                    case 200..<300:
                        NSLog("\(try! JSONSerialization.jsonObject(with: data!, options: []))")
                        if let data = data{
                            self.parseData(httpMethod: request.httpMethod!, data: data)
                            if let notes = self.notes{
                                DispatchQueue.main.async {
                                    switch request.httpMethod! {
                                    case "GET":
                                        self.delegate?.updateNotes(notes: notes, setting: .get)
                                    case "PUT":
                                        self.delegate?.updateNotes(notes: notes, setting: .put)
                                    default: break
                                    }
                                }
                            }else{
                                self.delegate?.updateNotes(notes: [], setting: .get)
                            }
                        }
                        self.requestQueue.removeFirst()
                    default:
                        DispatchQueue.main.async {
                            self.delegate?.updateNotes(notes: [], setting: .delete)
                        }
                    }
                    self.isFinish = true
                    if self.requestQueue.count != 0{
                        self.startTask()
                    }
                }
            }
            task.resume()
        }
    }
}

extension URLOperation: URLSessionDelegate{
}

