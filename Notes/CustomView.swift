//
//  CustomView.swift
//  Notes
//
//  Created by Алексей Лопух on 10.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit
internal protocol ColorPickerControllerDelegate: NSObjectProtocol{
    func setColor(color: UIColor)
    func setPoint(point: CGPoint)
}

class CustomView: UIView, ColorPickerControllerDelegate {
    
    let transitionDelegate = TransitionDelegate()
    
    var  isCheck: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var checkColor: UIColor? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    var pointColorPicker: CGPoint? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(2.0)
        if let color = checkColor {
            context.setFillColor(color.cgColor)
            context.fill(rect)
            
        }else{
            for y in stride(from: (0 as CGFloat), to: rect.height, by: 1.0){
                let saturation = 2.0 * CGFloat(rect.height - y) / rect.height
                for x in stride(from: (0 as CGFloat), to: rect.width, by: 1.0){
                    let hue = x / rect.width
                    let color = UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0)
                    context.setFillColor(color.cgColor)
                    context.fill(CGRect(x: x, y: y, width: 1.0, height: 1.0))
                }
                
            }
        }
        
        if isCheck{
            let minSize = min(rect.height / 3, rect.width / 3)
            let rectCircle = CGRect(x: rect.width / 3 * 2 - 3,
                                    y: 0,
                                    width: minSize,
                                    height: minSize)
            
            context.strokeEllipse(in: rectCircle)
            
            context.move(to: CGPoint(x: rectCircle.minX  + 1 , y: rectCircle.height / 2))
            context.addLine(to: CGPoint(x: rectCircle.minX + (rectCircle.width / 2) , y: rectCircle.maxY - 1))
            
            context.addLine(to: CGPoint(x: rectCircle.maxX - 3, y: rectCircle.minY + 3))
            context.strokePath()
            
        }
        
        context.stroke(rect)

    }
    func presentColorPicker(){
        let storyboard: UIStoryboard = UIStoryboard(name: "ColorPicker", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ColorPickerController") as! ColorPickerController
        let currentController = self.getCurrentViewController()
        guard let currentViewController = currentController else {return}
        if currentViewController.presentingViewController == nil {
            if let checkColor = self.checkColor {
                vc.color = checkColor
            }
            if let point = self.pointColorPicker{
                vc.point = point
            }
            vc.delegate = self
            
            vc.transitioningDelegate = self.transitionDelegate
            currentViewController.transitioningDelegate = self.transitionDelegate
            vc.modalPresentationStyle = .custom
            currentViewController.present(vc, animated: true, completion: nil)
        }
        
        
    }
    func getCurrentViewController() -> UIViewController? {
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
    
    func setColor(color: UIColor) {
        checkColor = color
    }
    
    func setPoint(point: CGPoint){
        pointColorPicker = point
    }
}
