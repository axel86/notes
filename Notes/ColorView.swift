//
//  ColorView.swift
//  Notes
//
//  Created by Алексей Лопух on 09.05.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit


@IBDesignable class ColorView: UIView {

    
    @IBOutlet weak var whiteView: ColorNote!
    @IBOutlet weak var greenView: ColorNote!
    @IBOutlet weak var redView: ColorNote!
    @IBOutlet weak var customView: CustomView!
    
    var isCheck: ColorCheck = .White
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize(){
        guard let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last else {
            return
        }
        guard let nibView = Bundle(for: type(of: self)).loadNibNamed(nibName, owner: self, options: nil)?.last as? UIView else {
            return
        }
        
        addSubview(nibView)
        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let tapRecognizerRed = UITapGestureRecognizer(target: self, action: #selector(self.clickRed))
        let tapRecognizerGreen = UITapGestureRecognizer(target: self, action: #selector(self.clickGreen))
        let tapRecognizerWhite = UITapGestureRecognizer(target: self, action: #selector(self.clickWhite))
        let tapRecognizerCustom = UITapGestureRecognizer(target: self, action: #selector(self.clickCustom))
        let longRecognizerCustom = UILongPressGestureRecognizer(target: self, action: #selector(self.longCheckCustom))
        longRecognizerCustom.minimumPressDuration = 0.3
        whiteView.addGestureRecognizer(tapRecognizerWhite)
        redView.addGestureRecognizer(tapRecognizerRed)
        greenView.addGestureRecognizer(tapRecognizerGreen)
        customView.addGestureRecognizer(tapRecognizerCustom)
        customView.addGestureRecognizer(longRecognizerCustom)
        
    }
    
    func clickRed(){
        notCheck()
        isCheck = .Red
        redView.isCheck = true
    }
    func clickGreen(){
        notCheck()
        isCheck = .Green
        greenView.isCheck = true
    }
    func clickWhite(){
        notCheck()
        isCheck = .White
        whiteView.isCheck = true
    }
    func clickCustom(){
        notCheck()
        isCheck = .Custom
        customView.isCheck = true
    }
    func longCheckCustom(){
        notCheck()
        isCheck = .Custom
        customView.isCheck = true
        customView.presentColorPicker()
    }
    
    func notCheck(){
        switch isCheck {
        case .Red: redView.isCheck = false
        case .Green: greenView.isCheck = false
        case .White: whiteView.isCheck = false
        case .Custom: customView.isCheck = false
        }
    }
    
    func getColor() -> UIColor{
        switch isCheck {
        case .Custom:
            guard let check = customView.checkColor else {
                return UIColor.white
            }
            return check
        case .Green:
            return UIColor.green
        case .Red:
            return UIColor.red
        case .White:
            return UIColor.white
        }
    }
    

}
