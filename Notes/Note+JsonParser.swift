//
//  Note+JsonParser.swift
//  Notes
//
//  Created by Алексей Лопух on 20.04.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import Foundation
import UIKit

extension Note {
    var json: [String:Any] {
        var js: [String:Any] = [:]
        js["title"] = title as Any
        js["content"] = content as Any
        js["uid"] = uuid as Any
        if color != UIColor.white {
            js["color"] = color.hexString() as Any
        }
        
        if let date = dateDestroy {
            js["destroy_date"] = Int(date.timeIntervalSince1970)
        }
        return js
    }
    
    static func parse(json:[String:Any]) -> Note? {
        guard let title = json["title"] as? String ,
            let content = json["content"] as? String else{
                return nil
        }
        
        var coloreNote = UIColor.white
        if let colore = json["color"] as? String {
            coloreNote = UIColor(hexString: colore)
        }
        var dateNote: Date? = nil
        if let date = json["destroy_date"] as? Double {
                dateNote = Date(timeIntervalSince1970: date)
        }
        
        return Note(title: title,
                    content: content,
                    uuid: (json["uid"] as? String ) ?? UUID().uuidString.lowercased(),
                    color: coloreNote,
                    dateDes: dateNote)
    }
}
