//
//  ViewController.swift
//  Notes
//
//  Created by Алексей Лопух on 20.04.17.
//  Copyright © 2017 Алексей Лопух. All rights reserved.
//

import UIKit

internal protocol NotesEditorDelegat: NSObjectProtocol{
    func addNewNote(newNote: Note)
}

class NotesEditor: UIViewController, UITextViewDelegate{
    weak internal var delegat: NotesEditorDelegat?
    
    
    @IBOutlet weak var constraintHightTextView: NSLayoutConstraint!
    @IBOutlet weak var constraintsColorViewToSwitch: NSLayoutConstraint!

    @IBOutlet weak var titleView: UITextField!
    @IBOutlet weak var contentView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var backgroundScrollView: UIScrollView!
    @IBOutlet weak var switchDateDestroy: UISwitch!
    @IBOutlet weak var colorView: ColorView!
        
    var note: Note!
    
    let lengthColorViewToSwitch:[Bool: CGFloat]  = [true: 278, false: 34]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.delegate = self
        
        if let _ = note{
            parseNote()
        }else{
            note = Note(title: "", content: "")
        }
        
        textViewDidChange(contentView)
        
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        backgroundScrollView.addGestureRecognizer(gestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide,  object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        if switchDateDestroy.isOn {
            backgroundScrollView.contentInset.bottom -= lengthColorViewToSwitch[true]!
        }else{
            backgroundScrollView.contentInset.bottom += lengthColorViewToSwitch[false]!
        }
    }
    
    func parseNote(){
        titleView.text = note.title
        contentView.text = note.content
        if let date = note.dateDestroy {
            switchDateDestroy.isOn = true
            datePicker.date = date
        }else{
            switchDateDestroy.isOn = false
        }
        datePickerHidden(switchDateDestroy)
        switch note.color{
            case UIColor.red:
                colorView.clickRed()
            case UIColor.green:
                colorView.clickGreen()
            case UIColor.white:
                colorView.clickWhite()
            default:
                colorView.clickCustom()
                colorView.customView.checkColor = note.color
        }
    }
    
    @IBAction func datePickerHidden(_ sender: Any) {
        guard let switchDatePicker = sender as? UISwitch else {return}
        let changeBottom = lengthColorViewToSwitch[true]!
        if switchDatePicker.isOn{
            datePicker.isEnabled = true
            datePicker.isHidden = false
            constraintsColorViewToSwitch.constant = lengthColorViewToSwitch[switchDatePicker.isOn]!
            backgroundScrollView.contentInset.bottom += changeBottom
        }else{
            datePicker.isHidden = true
            datePicker.isEnabled = false
            constraintsColorViewToSwitch.constant = lengthColorViewToSwitch[switchDatePicker.isOn]!
            backgroundScrollView.contentInset.bottom -= changeBottom
        }
    }
    
    func hideKeyboard(){
        self.view.endEditing(true)
    }
    func keyboardWillShow(notification:NSNotification) {
        adjustingHeigth(show: true, notification: notification)
    }
    
    func keyboardWillHide(notification:NSNotification) {
        adjustingHeigth(show: false, notification: notification)
    }
    
    func adjustingHeigth(show: Bool, notification: NSNotification){
        var userInfo = notification.userInfo!
        
        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let chagneInHeigth = keyboardFrame.height  * (show ? 1 : -1)
        
        backgroundScrollView.contentInset.bottom += chagneInHeigth
        
        backgroundScrollView.scrollIndicatorInsets.bottom += chagneInHeigth
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let textViewSize = contentView.sizeThatFits(CGSize(width: contentView.frame.size.width, height: .greatestFiniteMagnitude))
        constraintHightTextView.constant = textViewSize.height
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        
        super.viewWillDisappear(animated)
    }

    func creatNote() -> Note {
        return Note(title: titleView.text!,
                         content: contentView.text,
                         uuid: self.note.uuid,
                         color: colorView.getColor(),
                         dateDes: (switchDateDestroy.isOn) ? datePicker.date : nil)
    }
    @IBAction func save(_ sender: Any) {
        let newNote = creatNote()
        self.delegat?.addNewNote(newNote: newNote)
        self.navigationController?.popViewController(animated: true)
    }
    
}


